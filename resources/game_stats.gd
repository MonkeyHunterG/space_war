class_name GameStats
# Needed for ScoreComponent


extends Resource


# added a setget function at the end
@export var score: int = 0:
	set(value):
		score = value
		score_changed.emit(score)
@export var highscore: int = 0


signal score_changed(new_score)
