extends Node2D


@onready var visible_on_screen_notifier_2d: VisibleOnScreenNotifier2D = $VisibleOnScreenNotifier2D
@onready var flash_component:FlashComponent = $FlashComponent
@onready var scale_component:ScaleComponent = $ScaleComponent
@onready var hitbox_component:HitboxComponent = $HitboxComponent


func _ready():
	flash_component.flash()
	scale_component.tween_scale()
	visible_on_screen_notifier_2d.screen_exited.connect(queue_free)
	hitbox_component.hit_hurtbox.connect(
		# hit_hurtbox expects 1 argument
		# unbind(1) removes this dependence
		queue_free.unbind(1)
	)
