extends Node2D


@export var game_stats: GameStats
@export var GreenEnemyScene: PackedScene
@export var YellowEnemyScene: PackedScene
@export var PinkEnemyScene: PackedScene


@onready var spawner_component: SpawnerComponent = $SpawnerComponent
@onready var green_enemy_spawn_timer:Timer = $GreenEnemySpawnTimer
@onready var yellow_enemy_spawn_timer: Timer = $YellowEnemySpawnTimer
@onready var pink_enemy_spawn_timer: Timer = $PinkEnemySpawnTimer


const YELLOW_ENEMY_SCORE_THRESHOLD: int = 10
const PINK_ENEMY_SCORE_THRESHOLD: int = 50


var margin = 8
var screen_width = ProjectSettings.get_setting("display/window/size/viewport_width")


func _ready():
	green_enemy_spawn_timer.timeout.connect(handle_spawn.bind(GreenEnemyScene, green_enemy_spawn_timer))
	yellow_enemy_spawn_timer.timeout.connect(handle_spawn.bind(YellowEnemyScene, yellow_enemy_spawn_timer, 5.0))
	pink_enemy_spawn_timer.timeout.connect(handle_spawn.bind(PinkEnemyScene, pink_enemy_spawn_timer, 10.0))
	game_stats.score_changed.connect(func(new_score: int):
		if (yellow_enemy_spawn_timer.process_mode != Node.PROCESS_MODE_INHERIT):
			if (new_score >= YELLOW_ENEMY_SCORE_THRESHOLD):
				yellow_enemy_spawn_timer.process_mode = Node.PROCESS_MODE_INHERIT
		if (pink_enemy_spawn_timer.process_mode != Node.PROCESS_MODE_INHERIT):
			if (new_score >= PINK_ENEMY_SCORE_THRESHOLD):
				pink_enemy_spawn_timer.process_mode = Node.PROCESS_MODE_INHERIT
	)
	

func handle_spawn(enemy_scene: PackedScene, timer: Timer, time_offset: float = 2.0) -> void:
	spawner_component.scene = enemy_scene
	spawner_component.spawn(Vector2(randi_range(margin, screen_width - margin), -16))
	var spawn_rate = time_offset / (0.5 + (game_stats.score * 0.01))
	timer.start(spawn_rate * randf_range(0.25, 0.5))
