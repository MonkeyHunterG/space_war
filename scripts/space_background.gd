extends ParallaxBackground


@onready var background_layer = %BackgroundLayer
@onready var middleground_layer = %MiddlegroundLayer
@onready var foreground_layer = %ForegroundLayer


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# 60 frames
	foreground_layer.motion_offset.y += 20 * delta
	middleground_layer.motion_offset.y += 10 * delta
	background_layer.motion_offset.y += 2 * delta
