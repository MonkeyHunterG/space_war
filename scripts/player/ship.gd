extends Node2D


@onready var left_muzzle: Marker2D = $LeftMuzzle
@onready var right_muzzle: Marker2D = $RightMuzzle
@onready var spawner_component_projectile: SpawnerComponent = $SpawnerComponentProjectile
@onready var fire_rate_timer: Timer = $FireRateTimer
@onready var scale_component: ScaleComponent = $ScaleComponent
@onready var animated_sprite_2d: AnimatedSprite2D = $AnimatedSprite2D
@onready var move_component: MoveComponent = $MoveComponent
@onready var flame_animated_sprite:AnimatedSprite2D = $FlameAnimatedSprite
@onready var variable_pitch_audio_stream_player: VariablePitchAudioStreamPlayer = $VariablePitchAudioStreamPlayer


# Called when the node enters the scene tree for the first time.
func _ready():
	fire_rate_timer.timeout.connect(fire_lasers)


func fire_lasers() -> void:
	variable_pitch_audio_stream_player.play_with_variance()
	spawner_component_projectile.spawn(left_muzzle.global_position)
	spawner_component_projectile.spawn(right_muzzle.global_position)
	# If scaling after firing is wanted, uncomment below
	#scale_component.tween_scale()


func _process(_delta: float) -> void:
	animate_ship()


func animate_ship() -> void:
	if (move_component.velocity.x < 0):
		animated_sprite_2d.play("turn_left")
		flame_animated_sprite.play("turn_left")
	elif (move_component.velocity.x > 0):
		animated_sprite_2d.play("turn_right")
		flame_animated_sprite.play("turn_right")
	else:
		animated_sprite_2d.play("center")
		flame_animated_sprite.play("center")
