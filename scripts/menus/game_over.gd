extends Control


# also in menu
@export var game_stats: GameStats


# useful for control nodes
# since they often move around in the UI
@onready var score_value = %ScoreValue
@onready var highscore_value = %HighscoreValue


# save_path is consistent across multiple OSes
# should always be used for prodcution
# also in menu
const SAVE_PATH = "user://save.cfg"
# test_save_path only used for development and reference
# also in menu
const TEST_SAVE_PATH = "res://save.cfg"

# while developing only
# also in menu
var save_path = TEST_SAVE_PATH


func _ready() -> void:
	load_highscore()
	# the highscore is saved in singleton "resource_stash"
	# since it will be reset back to 0
	# once no other object references it
	# because then all values will be removed from memory
	if (game_stats.score > game_stats.highscore):
		game_stats.highscore = game_stats.score
		save_highscore()
	score_value.text = str(game_stats.score)
	highscore_value.text = str(game_stats.highscore)

func _process(_delta: float) -> void:
	if (Input.is_action_just_pressed("ui_accept")):
		game_stats.score = 0
		get_tree().change_scene_to_file("res://scenes/menus/menu.tscn")
	if (Input.is_action_just_pressed("ui_cancel")):
		get_tree().quit()


# also in menu
func load_highscore() -> void:
	var config = ConfigFile.new()
	var error = config.load(save_path)
	if error:
		return
	game_stats.highscore = config.get_value("game", "highscore")


func save_highscore() -> void:
	var config = ConfigFile.new()
	config.set_value("game", "highscore", game_stats.highscore)
	config.save(save_path)
